<?php


namespace app;


class Container
{
    protected $items = [];

    public function get($name)
    {
        return $this->items[$name];
    }

    public function set($name, $instance)
    {
        $this->items[$name] = $instance;
    }

    public function has($name){
        return isset($this->items[$name]);
    }

    public function make($name){
        if($this->has($name)){
            return $this->get($name);
        }
        return new $name();
    }
}
