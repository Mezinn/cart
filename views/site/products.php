<?php

use app\models\Cart;
use app\models\Product;

/**
 * @var Product[] $products
 * @var Cart $cart
 */

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>

<div class="container mt-5">
    <div class="row">
        <?php foreach ($products as $product) : ?>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><?= $product->getName() ?></h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                            the
                            card's content.</p>
                        <div class="d-flex justify-content-between">
                            <div><?= $product->getPrice() ?></div>
                            <div data-id="<?= $product->getId() ?>" class="d-flex justify-content-between flex-grow-1">
                                <div class="remove">-</div>
                                <div class="quantity-<?= $product->getId() ?>"><?= $cart->getCount($product) ?></div>
                                <div onclick="cartController.add(<?= $product->getId() ?>)" class="add">+</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
</body>
<script src="/web/index.js"></script>
</html>

