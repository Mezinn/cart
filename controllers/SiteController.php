<?php

namespace app\controllers;

use app\Container;
use app\models\Cart;
use app\models\CartManager;
use app\repositories\ProductRepository;
use app\ViewFactory;

class SiteController
{
    /** @var ViewFactory */
    protected $viewFactory;
    /** @var ProductRepository */
    protected $productRepository;
    /** @var CartManager */
    protected $cartManager;

    public function __construct(Container $container)
    {
        $this->viewFactory = new ViewFactory();
        $this->productRepository = $container->make(ProductRepository::class);
        $this->cartManager = $container->make(CartManager::class);
    }

    public function products(): string
    {
        $products = $this->productRepository->getProducts();

        return $this->viewFactory->render('site/products', [
            'products' => $products,
            'cart' => $this->cartManager->getCart()
        ]);
    }

    public function cart(): string
    {
        return $this->viewFactory->render('site/cart', [
            'cart' => $this->cartManager->getCart()
        ]);
    }
}
