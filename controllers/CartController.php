<?php

namespace app\controllers;

use app\Container;
use app\models\CartManager;
use app\repositories\ProductRepository;

class CartController
{
    /** @var ProductRepository */
    protected $productRepository;
    /** @var CartManager */
    protected $cartManager;

    public function __construct(Container $container)
    {
        $this->productRepository = $container->make(ProductRepository::class);
        $this->cartManager = $container->make(CartManager::class);
    }

    public function add()
    {
        $id = $_GET['id'];

        $product = $this->productRepository->get($id);

        $count = $this->cartManager->getCart()->add($product);

        $this->cartManager->saveCart();

        return json_encode($count);
    }

    public function remove()
    {
        $id = $_GET['id'];

        $product = $this->productRepository->get($id);

        $count = $this->cartManager->getCart()->remove($product);

        $this->cartManager->saveCart();

        return json_encode($count);
    }

}
