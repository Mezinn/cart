<?php

namespace app;

use Exception;

class Router
{
    protected $container;

    protected $routes = [];

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function get($name, $handler)
    {
        $this->routes['get'][$name] = $handler;
    }

    public function post($name, $handler)
    {
        $this->routes['post'][$name] = $handler;
    }

    public function resolve()
    {
        $url = explode('?', trim($_SERVER['REQUEST_URI'], '/'))[0];
        $method = strtolower($_SERVER['REQUEST_METHOD']);

        try {
            $handler = $this->routes[$method][$url];
        } catch (Exception $exception) {
            return 'Not found';
        }

        try {
            [$controllerName, $actionName] = $handler;

            $controller = new $controllerName($this->container);

            return $controller->$actionName();

        } catch (Exception $exception) {
            return 'Internal server error';
        }
    }
}
