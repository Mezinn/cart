<?php

namespace app\repositories;

use app\models\Product;

class ProductRepository
{
    public function getProducts()
    {
        return [
            '1' => new Product(1, 'Apple', 100),
            '2' => new Product(2, 'Netflix', 200),
            '3' => new Product(3, 'Amazon', 300),
            '4' => new Product(4, 'Turner', 400),
        ];
    }

    public function get($id)
    {
        return $this->getProducts()[$id];
    }
}
