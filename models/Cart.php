<?php

namespace app\models;

class Cart
{
    protected $products = [];

    public function has(Product $product)
    {
        return isset($this->products[$product->getId()]);
    }

    public function add(Product $product)
    {
        if ($this->has($product)) {

            $cartProduct = new CartProduct($product,
                $this->products[$product->getId()]->getQuantity() + 1);

            $this->products[$product->getId()] = $cartProduct;

            return $cartProduct->getQuantity();
        } else {

            $cartProduct = new CartProduct($product);

            $this->products[$product->getId()] = $cartProduct;

            return $cartProduct->getQuantity();
        }
    }

    public function remove(Product $product)
    {
        if ($this->has($product)) {
            if ($this->products[$product->getId()]->getQuantity() > 1) {
                $cartProduct = new CartProduct($product,
                    $this->products[$product->getId()]->getQuantity() - 1);
                $this->products[$product->getId()] = $cartProduct;

                return $cartProduct->getQuantity();
            } else {
                unset($this->products[$product->getId()]);
            }
        }
        return 0;
    }

    public function getProducts()
    {
        return array_map(function ($cartProduct) {
            return $cartProduct->getProduct();
        }, $this->products);
    }

    public function getCount(Product $product)
    {
        if ($this->has($product)) {
            return $this->products[$product->getId()]->getQuantity();
        }
        return 0;
    }
}
