<?php

namespace app\models;

class CartProduct
{
    /** @var Product */
    protected $product;

    protected $quantity;

    public function __construct(Product $product, $quantity = 1)
    {
        $this->product = $product;
        $this->quantity = $quantity;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getPrice()
    {
        return $this->product->getPrice();
    }

    public function getTotal()
    {
        return $this->getPrice() * $this->quantity;
    }
}
