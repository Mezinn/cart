<?php

namespace app\models;

class CartManager
{
    const SESSION_KEY = 'session_cart';

    protected $cart;

    public function saveCart()
    {
        if ($this->cart !== null) {
            $_SESSION[self::SESSION_KEY] = serialize($this->cart);
        }
    }

    public function getCart(): Cart
    {
        if ($this->cart === null) {

            if (isset($_SESSION[self::SESSION_KEY])) {
                $this->cart = unserialize($_SESSION[self::SESSION_KEY]);
            }else{
                $this->cart = new Cart();
            }
        }

        return $this->cart;
    }
}
