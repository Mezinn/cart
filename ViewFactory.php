<?php

namespace app;

class ViewFactory
{
    public function render($view, $payload = []): string
    {
        $view = trim($view, '/');

        extract($payload);

        ob_start();

        include __DIR__ . "/views/{$view}.php";

        return ob_get_clean();
    }
}
