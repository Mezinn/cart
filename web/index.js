const cartController = {

    add: function (id) {
        cartModel.add(id);
    },

    remove: function (id) {

    }
}

const cartModel = {

    products: new Map(),
    listeners: new Set(),

    add(id) {
        fetch(`http://localhost/cart/add?id=${id}`)
            .then(res => res.json())
            .then(count => {
                this.products.set(id, count)
                this.change({id, quantity: count});
            });
    },
    getCount(id) {
        return this.products.get(id);
    },

    onChange(handler) {
        this.listeners.add(handler);
    },

    change(model) {
        this.listeners.forEach(handler => handler(model));
    }
}

cartModel.onChange(function (model) {
    document.querySelector(`.quantity-${model.id}`).innerHTML = model.quantity;
});




