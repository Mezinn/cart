<?php

use app\Container;
use app\controllers\SiteController;
use app\models\Cart;
use app\models\CartManager;
use app\Router;
use app\controllers\CartController;

require_once __DIR__ . '/vendor/autoload.php';

session_start();

$container = new Container();

$cartManager = new CartManager();

$router = new Router($container);

$router->get('products', [SiteController::class, 'products']);
$router->get('cart', [SiteController::class, 'cart']);

$router->get('cart/add', [CartController::class, 'add']);
$router->get('cart/remove', [CartController::class, 'remove']);

echo $router->resolve();


